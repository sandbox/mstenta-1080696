/**
 * Display the results of the customer search.
 * This is copied directly from uc_order.js and modified to include the Company field.
 */
function uc_order_company_search_load_customer_search_results() {
  var first_name = $('#customer-select #edit-first-name').val();
  var last_name = $('#customer-select #edit-last-name').val();
  var email = $('#customer-select #edit-email').val();
  var company = $('#customer-select #edit-company').val();
  
  if (first_name == '') {
    first_name = '0';
  }
  if (last_name == '') {
    last_name = '0';
  }
  if (email == '') {
    email = '0';
  }
  if (company == '') {
    company = '0';
  }

  $.post(Drupal.settings.ucURL.adminOrders + 'customer/search/' + encodeURIComponent(first_name) + '/' + encodeURIComponent(last_name) + '/' + encodeURIComponent(email) + '/' + encodeURIComponent(company),
         { },
         function (contents) {
           $('#customer-select').empty().append(contents);
         }
  );
  return false;
}